#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <vector>
#include <sstream> 
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <chrono>
#include <string>
#include "mpi.h"
#include "minHeap.cpp"
#include "../plugin/dcdplugin.c"


int get_length(std::string indices_list){

    std::vector <std::string> vect;
    std::size_t found;
    int start, end, count = 0;
    std::string temp;
    std::stringstream ss(indices_list);

    while(getline(ss, temp, ',')) {
        vect.push_back(temp); 
    }
    for(int i = 0; i < vect.size(); i++){
        found  = vect[i].find('-');
        if (found!=std::string::npos){
            start = stoi(vect[i].substr(0,found));
            end = stoi(vect[i].substr(found+1, vect[i].size()-1));

            for (int i = start; i <= end; ++i){
                count++;
            }
        }
        else{
            count++;
        }
    }
    return count;
}

void get_all_indices(std::string indices_list, int arr [], int n){
    std::vector <std::string> vect;
    std::vector <int> output;
    std::size_t found;
    int start, end, offset, counter = 0;
    std::string temp;

    std::stringstream ss(indices_list);
    
    while(getline(ss, temp, ',')) {
        vect.push_back(temp); 
    } 
      
    for(int i = 0; i < vect.size(); i++){
        found  = vect[i].find('-');
        if (found!=std::string::npos){
            start = stoi(vect[i].substr(0,found));
            end = stoi(vect[i].substr(found+1, vect[i].size()-1));

            offset = 0;
            while(counter < n){
                arr[counter] = start+offset;

                if(start+offset == end+1){
                    break;
                }
                else{
                    offset++;
                }
                counter++;
            }
        }
        else if (counter < n){
            arr[counter] = stoi(vect[i]);
            counter++;
        }
    }
}

int main(int argc, char* argv[])
{
    int id, nodenum, receiver;      //holds process's rank id
    int count=0;            
    const auto arg = std::string{"-i"};
    std::vector<int> vect;
    int na, nb;
    std::ofstream filestream;
    molfile_timestep_t timestep;
    void *v;
    dcdhandle *dcd;
    std::string arr[4];
    int natoms, arrSize = 0;

    
    MPI_Init(&argc, &argv);                     //Start MPI
    MPI_Comm_rank(MPI_COMM_WORLD, &id);         //get rank of node's process
    MPI_Comm_size(MPI_COMM_WORLD, &nodenum);
    
    if(id == 0){          //master thread reads data and allocates time steps to threads
        
        for (int i = 0; i < argc; ++i){
            if(i >0 && argv[i-1] == arg){

                std::string x;
                std::ifstream inFile;

                inFile.open(argv[i]);
                if (!inFile) {
                    std::cout << "Unable to open file";
                    exit(1); // terminate with error
                }

                while (inFile >> x) {
                    if(inFile.eof()){
                        arr[arrSize++] = x;
                        break;
                    }
                    arr[arrSize++] = x;
                }
                inFile.close(); 
            } 
        }

        std::string path_to_char = *(arr);
        
        int k = stoi(*(arr+1));

        std::string set_a = *(arr+2);
        na = get_length(set_a);
        
        std::string set_b = *(arr+3);
        nb = get_length(set_b);
        
        int a [na], b [nb];

        get_all_indices(set_a, a, na);
        get_all_indices(set_b, b, nb);

        const char *path = path_to_char.c_str();
        v = open_dcd_read(path, "dcd", &natoms);
        if (!v) {
          fprintf(stderr, "main) open_dcd_read failed for file %s\n", *argv);
          return 1;
        }
        
        dcd = (dcdhandle *)v;
        molfile_timestep_t timestep; // create new time step each time so the the each thread receives a different ref

        timestep.coords = (float *)malloc(3*sizeof(float)*natoms);

        int timestep_size = (5*na*nb);

        //filestream.open("data/mpi_results.txt");
        
        for (int i=0; i<dcd->nsets; i++) {
            float x, y, z;
            float coords [timestep_size];
            int num_timesteps;
            int rc = read_next_timestep(v, natoms, &timestep);
            
            for (int i = 0; i < na; i++){
                for (int j = 0; j < nb; j++){
                    x = *(timestep.coords + (a[i]*3)+0) - *(timestep.coords + (b[j]*3)+0);
                    y = *(timestep.coords + (a[i]*3)+1) - *(timestep.coords + (b[j]*3)+1);
                    z = *(timestep.coords + (a[i]*3)+2) - *(timestep.coords + (b[j]*3)+2);

                    coords[(i*nb*5)+0+(j*5)] = x;
                    coords[(i*nb*5)+1+(j*5)] = y;
                    coords[(i*nb*5)+2+(j*5)] = z;
                    coords[(i*nb*5)+3+(j*5)] = a[i];
                    coords[(i*nb*5)+4+(j*5)] = b[j];

                    //std::cout << x <<"\n";
                }
            }

            receiver = 1; // for testing on laptop

            if (nodenum >= 4){
                 if(i%nodenum+1 < nodenum){
                    receiver = i%nodenum+1;  // offset by 2 
                }

                // else{
                //      std::cout << "reciever is " << receiver << "\n";
                //     receiver = i%nodenum+1;
                // }
            }
            num_timesteps = 10;
            //std::cout << "number of threads " << nodenum << "\n";

            MPI_Send(&na, 1, MPI_INT, receiver, 0, MPI_COMM_WORLD);
            MPI_Send(&nb, 1, MPI_INT, receiver, 0, MPI_COMM_WORLD);

            MPI_Send(&timestep_size, 1, MPI_INT, receiver, 0, MPI_COMM_WORLD);

            MPI_Send(coords, timestep_size, MPI_FLOAT, receiver, 1, MPI_COMM_WORLD);

            if (rc) {
                fprintf(stderr, "error in read_next_timestep on frame %d\n", i);
                return 1;
            }

            if(i == 10){
                break;
            }
        }
        close_file_read(v);
    }

    else if(id!=0){ // worker threads for performing the calculations

        int na, nb, timestep_size; 

        MPI_Recv(&na, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        
        ///std::cout << "thread " << id <<" working \n";
        int a [na], b [nb]; 

        MPI_Recv(&nb, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(&timestep_size, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        float coords[timestep_size]; 
        
        MPI_Recv(&coords, timestep_size, MPI_FLOAT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        MinHeap heap (na * nb);

        for (int i = 0; i < (timestep_size); i++){  // * by 5 because the it is now [x1, y1, z1, a, b]
            Distance_Pair pair;
            pair.distance = sqrt(pow(coords[i*5],2) + pow(coords[(i*5)+1],2) + pow(coords[(i*5)+2],2));
            pair.index_a = coords[(i*5)+3];
            pair.index_b = coords[(i*5)+4];
            heap.insertKey(pair);
        }

        ///std::cout << "started working \n";
        
        Distance_Pair pair;
         pair = heap.extractMin();
         std::cout << pair.index_a << ", "  << pair.index_b << ", "  << pair.distance << "\n";

         pair = heap.extractMin();
         std::cout << pair.index_a << ", "  << pair.index_b << ", "  << pair.distance << "\n";

         pair = heap.extractMin();
         std::cout << pair.index_a << ", "  << pair.index_b << ", "  << pair.distance << "\n";

        //  pair = heap.extractMin();
        //  std::cout << pair.index_a << ", "  << pair.index_b << ", "  << pair.distance << "\n";
        // for(int i = 0 ; i < 1 ; i++){
            // Distance_Pair pair;
            // pair = heap.extractMin();
            // std::cout << pair.index_a << ", "  << pair.index_b << ", "  << pair.distance << "\n";
        //}   
        ///if(id == 1){
            //   for(int k = 0; k < 5; k++){
            // Distance_Pair pair = heap.extractMin();
            // std::cout << pair.index_a << ", "  << pair.index_b << ", "  << pair.distance << "\n"; 
           // }
        // }
      
     }  
    MPI_Finalize();
    return 0;
}