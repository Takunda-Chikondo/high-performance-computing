#include <iostream>
#include <climits> 
#include "distancePair.cpp"

class MinHeap 
{ 
    Distance_Pair *harr; // pointer to array of elements in heap 
    int capacity; // maximum possible size of min heap 
    int heap_size; // Current number of elements in min heap 
public: 
    // Constructor 
    MinHeap(int capacity); 
  
    // to heapify a subtree with the root at given index 
    void MinHeapify(int ); 
  
    int parent(int i) { return (i-1)/2; } 
  
    // to get index of left child of node at index i 
    int left(int i) { return (2*i + 1); } 
  
    // to get index of right child of node at index i 
    int right(int i) { return (2*i + 2); } 
  
    // to extract the root which is the minimum element 
    Distance_Pair extractMin(); 
  
    // Decreases key value of key at index i to new_val 
    //void decreaseKey(int i, int new_val, int index_a, int index_b); 
  
    // Returns the minimum key (key at root) from min heap 
    Distance_Pair getMin();
  
    // Deletes a key stored at index i 
    void deleteKey(int i); 
  
    // Inserts a new key 'k' 
    void insertKey(Distance_Pair k); 

    void swap(Distance_Pair *x, Distance_Pair *y);
}; 
  
// Constructor: Builds a heap from a given array a[] of given size 
MinHeap::MinHeap(int cap) 
{ 
    heap_size = 0; 
    capacity = cap; 
    harr = new Distance_Pair[cap]; 
} 
  
// Inserts a new key 'k' 
void MinHeap::insertKey(Distance_Pair k) 
{ 
    if (heap_size == capacity) 
    { 
        //cout << "\nOverflow: Could not insertKey\n"; 
        return; 
    } 
  
    // First insert the new key at the end 
    heap_size++; 
    int i = heap_size - 1; 
    harr[i] = k; 
  
    // Fix the min heap property if it is violated 
    while (i != 0 && harr[parent(i)].distance > harr[i].distance) 
    { 
       swap(&harr[i], &harr[parent(i)]); 
       i = parent(i); 
    } 
} 
  
// Method to remove minimum element (or root) from min heap 
Distance_Pair MinHeap::extractMin() 
{ 
    Distance_Pair flag;
    if (heap_size <= 0){
        flag.distance = INT_MAX;
        flag.index_a = INT_MAX;
        flag.index_b = INT_MAX;
        return flag; 
    }

    if (heap_size == 1){ 
        heap_size--;
        return harr[0]; 
    } 
  
    // Store the minimum value, and remove it from heap 
    Distance_Pair root = harr[0]; 
    harr[0] = harr[heap_size-1]; 
    heap_size--; 
    MinHeapify(0); 
  
    return root; 
} 
  
  
// This function deletes key at index i. It first reduced value to minus 
// infinite, then calls extractMin() 
void MinHeap::deleteKey(int i) 
{ 
    //decreaseKey(i, INT_MIN, harr[i].index_a, harr[i].index_b); 
    extractMin(); 
} 
  
// A recursive method to heapify a subtree with the root at given index 
// This method assumes that the subtrees are already heapified 
void MinHeap::MinHeapify(int i) 
{ 
    int l = left(i); 
    int r = right(i); 
    int smallest = i; 
    if (l < heap_size && harr[l].distance < harr[i].distance) 
        smallest = l; 
    if (r < heap_size && harr[r].distance < harr[smallest].distance) 
        smallest = r; 
    if (smallest != i) 
    { 
        swap(&harr[i], &harr[smallest]); 
        MinHeapify(smallest); 
    } 
} 
  
// A utility function to swap two elements 
void MinHeap::swap(Distance_Pair *x, Distance_Pair *y) 
{ 
    Distance_Pair temp = *x; 
    *x = *y; 
    *y = temp; 
} 

 Distance_Pair  MinHeap::getMin() { 
    Distance_Pair min = harr[0];
    //deleteKey(harr[0]);
    return min; 
} 

// int main() 
// { 
//     MinHeap h(11);

//     Distance_Pair a, b, c, d, e, f, g;

//     a.distance = 3;
//     a.index_a = 5;
//     a.index_b = 5;

//     b.distance = 25;
//     b.index_a = 5;
//     b.index_b = 5;

//     c.distance = 1;
//     c.index_a = 5;
//     c.index_b = 5;
    
//     d.distance = 15;
//     d.index_a = 5;
//     d.index_b = 5;

//     e.distance = 5;
//     e.index_a = 5;
//     e.index_b = 5;
    
//     f.distance = 5;
//     f.index_a = 5;
//     f.index_b = 5;

//     g.distance = 4;
//     g.index_a = 5;
//     g.index_b = 5;

//     h.insertKey(a); 
//     h.insertKey(b);
//     h.insertKey(c); 
//     h.deleteKey(c); 
//     h.insertKey(d); 
//     h.insertKey(e); 
//     h.insertKey(f); 
//     h.insertKey(g);  
//     std::cout << h.getMin().distance << "\n"; 
//     return 0; 
// }
 