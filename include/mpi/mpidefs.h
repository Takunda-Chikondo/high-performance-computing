#ifndef MPI_DEFS
#define MPI_DEFS
typedef int MPI_Fint;
typedef int MPI_Aint;

#define ROMIO 1
#if ROMIO == 1
#include "mpio.h"
#endif

#endif