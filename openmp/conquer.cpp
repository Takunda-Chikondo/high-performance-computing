#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <vector>
#include <sstream> 
#include <algorithm>
#include <math.h>
#include <limits>
#include <cfloat>
#include <array>
#include <chrono>
#include "dcdplugin.c"

using namespace std;


class Point {
  public:
  int index;
  double x, y, z;
};

class Distance_pair{
  public:
    double distance;
    int index_a, index_b;
};

bool compare_distances(Distance_pair a, Distance_pair b) { 
    return a.distance < b.distance; 
} 

double dist(Point p1, Point p2) { 
    return sqrt((p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y) + (p1.z - p2.z)*(p1.z - p2.z)); 
} 

int bruteForce(Point P[], Point a[], vector<Distance_pair> &r, int n, int na) { 
    Distance_pair res;
    for (int i = 0; i < na; i++){
      for (int j = 0; j < n; j++){
          res.distance = dist(a[i],P[j]);
          res.index_a = a[i].index;
          res.index_b = P[j].index;
          r.push_back(res);
      }
    }
    return 0; 
} 
  
int closest(Point P[], Point a[], vector<Distance_pair> &r, int n, int na) { 
    if (n <= 1){
      
      return bruteForce (P, a, r, n, na); 
    }
    int mid = n/2; 
    
    closest(P, a, r, mid, na);
    closest(P + mid, a, r, n-mid, na);
    return 0;
} 
  
vector<int> get_all_indices(string indices_list){
  vector <string> vect;
  vector <int> output;
  std::size_t found;
  int start, end;
  string temp;

    std::stringstream ss(indices_list);
    
    while(getline(ss, temp, ',')) { 
        vect.push_back(temp); 
    } 
      
    for(int i = 0; i < vect.size(); i++){
      found  = vect[i].find('-');
      if (found!=std::string::npos){
        start = stoi(vect[i].substr(0,found));
        end = stoi(vect[i].substr(found+1, vect[i].size()-1));

        for (int i = start; i <= end; ++i){
          output.push_back(i);
        }
      }
      else{
        output.push_back(stoi(vect[i]));
      }
    }
    return output;
}

vector<vector<Point>> get_all_points(vector<int> vect_a, vector<int> vect_b, float *coords){
  double x, y, z;
  Point current_point;
  vector <vector<Point>> points;
  vector <Point> a, b;

  #pragma omp parallel for
  for (int i = 0; i < vect_a.size(); i++){
    current_point.index = vect_a[i];
    current_point.x = *(coords + vect_a[i]*3);
    current_point.y = *(coords + (vect_a[i]*3)+1);
    current_point.z = *(coords + (vect_a[i]*3)+2);
    a.push_back(current_point);

  for (int i = 0; i < vect_b.size(); i++){
    current_point.index = vect_b[i];
    current_point.x = *(coords + vect_b[i]*3);
    current_point.y = *(coords + (vect_b[i]*3)+1);
    current_point.z = *(coords + (vect_b[i]*3)+2);
    b.push_back(current_point);
    }
  }
  points.push_back(a);
  points.push_back(b);
  return points;
}

int main(int argc, char *argv[]) {
  const auto arg = std::string{"-i"};
  vector<int> vect_a, vect_b;
  vector <vector<Point>> points;
  vector <vector<Distance_pair>> distances;
  vector <Distance_pair> res;
  Point *a, *b;
  molfile_timestep_t timestep;
  void *v;
  dcdhandle *dcd;
  string x, arr[4];
  ifstream inFile;
  int natoms, arrSize = 0;

    for (int i = 0; i < argc; ++i){
      if(i >0 && argv[i-1] == arg){

      inFile.open(argv[i]);
      if (!inFile) {
        cout << "Unable to open file";
        exit(1); // terminate with error
      }

      while (inFile >> x) {
        if(inFile.eof()){
          arr[arrSize++] = x;
          break;
        }
        arr[arrSize++] = x;
      }
      inFile.close(); 
      } 
    }

    string path_to_char = *(arr);
    int k = stoi(*(arr+1));
    string a_indices = *(arr+2);
    string b_indices = *(arr+3);

    vect_a = get_all_indices(a_indices);
    vect_b = get_all_indices(b_indices);
    int n, na; 
  
  const char *path = path_to_char.c_str();
  v = open_dcd_read(path, "dcd", &natoms);
  if (!v) {
      fprintf(stderr, "main) open_dcd_read failed for file %s\n", *argv);
      return 1;
    }

    auto start = chrono::steady_clock::now();
    dcd = (dcdhandle *)v;
    timestep.coords = (float *)malloc(3*sizeof(float)*natoms);
      
    for (int i=0; i<dcd->nsets; i++) {
        int rc = read_next_timestep(v, natoms, &timestep);
        points = get_all_points(vect_a, vect_b, timestep.coords);
        n = vect_b.size();
        na = vect_a.size();
        
        a = &points[0][0];
        b = &points[1][0];
        
        closest(b, a, res, n, na);
        sort(res.begin(),res.end(), compare_distances);
        distances.push_back(res);
       
        res.clear();
        points.clear();
    
        if (rc) {
          fprintf(stderr, "error in read_next_timestep on frame %d\n", i);
          return 1;
        }
    }
    close_file_read(v);

    ofstream filestream;
    filestream.open("results1.txt");

    for (int i = 0; i < distances.size(); i++){
      for (int j = 0; j < k; j++){
      filestream  << i 
          << ", "<< distances[i][j].index_a << ", "<< distances[i][j].index_b << ", "<< distances[i][j].distance << "\n";
      }
    }
    
    filestream.close(); 
    auto end = chrono::steady_clock::now();
    cout << "Elapsed time in seconds: "
          <<chrono::duration_cast<chrono::milliseconds>(end-start).count()
          <<" sec";

    return 0;
}