#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <vector>
#include <sstream> 
#include <algorithm>
#include <math.h>
#include <chrono>
#include "/plugin/dcdplugin.c"

using namespace std;


class distance_pairs {
  public:
  double distance;
  int index_a;
  int index_b;
};
// helper methods
vector<int> get_all_indices(string indices_list){
	vector <string> vect;
	vector <int> output;
	std::size_t found;
	int start, end;
  string temp;

    std::stringstream ss(indices_list);
    
    while(getline(ss, temp, ',')) { 
        vect.push_back(temp); 
    } 
      
    for(int i = 0; i < vect.size(); i++){
    	found  = vect[i].find('-');
    	if (found!=std::string::npos){
    		start = stoi(vect[i].substr(0,found));
    		end = stoi(vect[i].substr(found+1, vect[i].size()-1));

    		for (int i = start; i <= end; ++i){
    			output.push_back(i);
    		}
    	}
    	else{
    		output.push_back(stoi(vect[i]));
    	}
    }
    return output;
}
bool compare_distances(distance_pairs a, distance_pairs b) { 
    return a.distance < b.distance; 
} 
vector<distance_pairs> get_min_distances(vector<distance_pairs> distances, int k){
  vector<distance_pairs> min_distances;
	sort(distances.begin(),distances.end(), compare_distances);
	for (int i = 0; i < k; i++){
		min_distances.push_back(distances[i]);
	}
  return min_distances;
}

vector<distance_pairs> get_all_distances(vector<int> &set_a, vector<int> &set_b, float *coords){
	vector<distance_pairs> distances;
	double x, y, z;
	distance_pairs current_pair;

	for (int i = 0; i < set_a.size(); i++){
		for (int j = 0; j < set_b.size(); j++){

			x = *(coords + set_a[i]*3) - *(coords + set_b[j]*3);
			y = *(coords + (set_a[i]*3)+1) - *(coords + (set_b[j]*3)+1);
			z = *(coords + (set_a[i]*3)+2) - *(coords + (set_b[j]*3)+2);
			current_pair.distance = sqrt(x*x + y*y + z*z);
			current_pair.index_a = set_a[i];
			current_pair.index_b = set_b[j];

			distances.push_back(current_pair);
		}

	}
  return distances;
}

int main(int argc, char *argv[]) {
	const auto arg = std::string{"-i"};
	vector<int> set_a, set_b;
	molfile_timestep_t timestep;
	void *v;
	dcdhandle *dcd;
	vector <distance_pairs> min_distances;
  vector <distance_pairs> distances;
	string arr[4];
	int natoms, arrSize = 0;

	
    for (int i = 0; i < argc; ++i){
    	if(i >0 && argv[i-1] == arg){

    	string x;
			ifstream inFile;

			inFile.open(argv[i]);
			if (!inFile) {
				cout << "Unable to open file";
				exit(1); // terminate with error
			}

			while (inFile >> x) {
				if(inFile.eof()){
					arr[arrSize++] = x;
					break;
				}
				arr[arrSize++] = x;
			}
			inFile.close(); 
		} 
    }

    string path_to_char = *(arr);
    int k = stoi(*(arr+1));
    string a_indices = *(arr+2);
    string b_indices = *(arr+3);

    set_a = get_all_indices(a_indices);
    set_b = get_all_indices(b_indices);
 	
	const char *path = path_to_char.c_str();
	v = open_dcd_read(path, "dcd", &natoms);
	if (!v) {
      fprintf(stderr, "main) open_dcd_read failed for file %s\n", *argv);
      return 1;
    }

    dcd = (dcdhandle *)v;
    vector <molfile_timestep_t> steps;
    auto start = chrono::steady_clock::now();

    ofstream filestream;
    filestream.open("results.txt");

    for (int i=0; i<dcd->nsets; i++) {
 		    molfile_timestep_t timestep;
        timestep.coords = (float *)malloc(3*sizeof(float)*natoms);
      	int rc = read_next_timestep(v, natoms, &timestep);
        distances = get_all_distances(set_a, set_b, timestep.coords);
        min_distances = get_min_distances(distances, k);
        for (int j = 0; j < k; j++){
          filestream  << i << ", "<< min_distances[j].index_a << ", "<< min_distances[j].index_b << ", "<< min_distances[j].distance << "\n";
        }    
        //min_distances.push_back(get_min_distances(distances, k));
      	//steps.push_back(timestep);
  		  if (rc) {
	        fprintf(stderr, "error in read_next_timestep on frame %d\n", i);
	        return 1;
      	}
  	}
      
    //  for (int i = 0; i < steps.size(); i++){
    //   distances = get_all_distances(set_a, set_b, steps[i].coords);     
    //   min_distances.push_back(get_min_distances(distances, k));
    // }
    close_file_read(v);
  	
   

    // ofstream filestream;
    // filestream.open("results.txt");
    
    // for (int i = 0; i < min_distances.size(); i++){
    //   for (int j = 0; j < k; j++){
    //   filestream  << i << ", "<< min_distances[i][j].index_a << ", "<< min_distances[i][j].index_b << ", "<< min_distances[i][j].distance << "\n";
    //   }
    // }

    filestream.close();
    auto end = chrono::steady_clock::now();
    cout << "Elapsed time in seconds: "
          <<chrono::duration_cast<chrono::milliseconds>(end-start).count()
          <<" sec";

    return 0;
}
