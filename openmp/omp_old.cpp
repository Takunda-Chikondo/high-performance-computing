#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <vector>
#include <sstream> 
#include <algorithm>
#include <math.h>
#include <limits>
#include <cfloat>
#include <array>
#include <chrono>
#include <omp.h>
#include "dcdplugin.c"

using namespace std;


class Point {
  public:
  int index;
  double x, y, z;
};

class Distance_pair{
  public:
    double distance;
    int index_a, index_b;
};

bool compare_distances(Distance_pair a, Distance_pair b) { 
    return a.distance < b.distance; 
} 

double dist(Point p1, Point p2) { 
    return sqrt((p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y) + (p1.z - p2.z)*(p1.z - p2.z)); 
} 

Point createPoint(int index, float *coords){
  Point point;

  point.index = index;
  point.x = *(coords + index*3);
  point.y = *(coords + (index*3)+1);
  point.z = *(coords + (index*3)+2);
  return point;
}

vector<Distance_pair> bruteForce(Point a[], int b[], int na, int nb, float *coords) { 
    Distance_pair res;
    vector<Distance_pair> r;
    
   #pragma omp parallel for firstprivate(r, res, a, b)
    for (int i = 0; i < na; i++){
      for (int j = 0; j < nb; j++){
          res.distance = dist(a[i],createPoint(b[j],coords));
          res.index_a = a[i].index;
          res.index_b = b[j];
          //#pragma omp critical
          r.push_back(res);
      }
    }
    return r; 
} 
  //closest(a, b, na, nb, timestep.coords);
vector<Distance_pair> closest(Point a[], int b[], int na, int nb, float *coords) { 
    if (nb <= 1){
      
      return bruteForce (a, b, na, nb, coords); 
    }
    int mid = nb/2; 
    
    vector <Distance_pair> dl;
    vector <Distance_pair> dr;
    #pragma omp parallel
    #pragma omp single nowait
    {
      #pragma omp task
      dl = closest(a, b, na, mid, coords);
      #pragma omp task
      dr = closest(a, b + mid, na, nb-mid, coords);
      #pragma omp taskwait
    }
    dl.insert(dl.end(), dr.begin(), dr.end());
    return dl;
} 
  
vector<int> get_all_indices(string indices_list){
  vector <string> vect;
  vector <int> output;
  std::size_t found;
  int start, end;
  string temp;

    std::stringstream ss(indices_list);
    
    while(getline(ss, temp, ',')) { 
        vect.push_back(temp); 
    } 
      
    for(int i = 0; i < vect.size(); i++){
      found  = vect[i].find('-');
      if (found!=std::string::npos){
        start = stoi(vect[i].substr(0,found));
        end = stoi(vect[i].substr(found+1, vect[i].size()-1));

        for (int i = start; i <= end; ++i){
          output.push_back(i);
        }
      }
      else{
        output.push_back(stoi(vect[i]));
      }
    }
    return output;
}

vector<Point> get_all_points(vector<int> vect_a, float *coords){
  double x, y, z;
  Point current_point;
  vector <Point> a;

    for (int i = 0; i < vect_a.size(); i++){
      current_point.index = vect_a[i];
      current_point.x = *(coords + vect_a[i]*3);
      current_point.y = *(coords + (vect_a[i]*3)+1);
      current_point.z = *(coords + (vect_a[i]*3)+2);
      a.push_back(current_point);
  }
  return a;
}

int main(int argc, char *argv[]) {
  const auto arg = std::string{"-i"};
  vector<int> vect_a, vect_b;
  int *b;
  Point *a;
  vector <Distance_pair> res;
  vector <Point> points;
  vector <vector<Distance_pair>> distances;
  
  void *v;
  dcdhandle *dcd;
  string x, arr[4];
  ifstream inFile;
  int natoms, arrSize = 0;

    for (int i = 0; i < argc; ++i){
      if(i >0 && argv[i-1] == arg){

      inFile.open(argv[i]);
      if (!inFile) {
        cout << "Unable to open file";
        exit(1); // terminate with error
      }

      while (inFile >> x) {
        if(inFile.eof()){
          arr[arrSize++] = x;
          break;
        }
        arr[arrSize++] = x;
      }
      inFile.close(); 
      } 
    }

    string path_to_char = *(arr);
    int k = stoi(*(arr+1));
    string a_indices = *(arr+2);
    string b_indices = *(arr+3);

    #pragma omp parallel
    #pragma omp single nowait
    {
      #pragma omp task
      vect_a = get_all_indices(a_indices);
      #pragma omp task
      vect_b = get_all_indices(b_indices);
      #pragma omp taskwait
    }
  
  int na = vect_a.size();
  int nb = vect_b.size();
  vector<molfile_timestep_t> timesteps;
  ofstream filestream;
  filestream.open("results1.txt");

  auto start = chrono::steady_clock::now();
  const char *path = path_to_char.c_str();
  v = open_dcd_read(path, "dcd", &natoms);
  if (!v) {
      fprintf(stderr, "main) open_dcd_read failed for file %s\n", *argv);
      return 1;
    }

    dcd = (dcdhandle *)v;
    molfile_timestep_t timestep;

    for (int i=0; i<dcd->nsets; i++) {
        timestep.coords = (float *)malloc(3*sizeof(float)*natoms);
        int rc = read_next_timestep(v, natoms, &timestep);
        
        //points = get_all_points(vect_a, vect_b, timestep.coords);
        points = get_all_points(vect_a, timestep.coords);
        a = &points[0];  // pre calculated the points for a to avoid recalculaiton
        b = &vect_b[0];
        
        res = closest(a, b, na, nb, timestep.coords);
        sort(res.begin(),res.end(), compare_distances);

        for (int j = 0; j < k; j++){
        filestream  << i 
            << ", "<< res[j].index_a << ", "<< res[j].index_b << ", "<< res[j].distance << "\n";
        }       
        res.clear();
        
        if (rc) {
          fprintf(stderr, "error in read_next_timestep on frame %d\n", i);
          return 1;
        }
    }
    close_file_read(v);

    //#pragma omp parallel for ordered private(points, a, b, res)
    //for (int i = 0; i < timesteps.size(); i++){
  
   
    // #pragma omp parallel for ordered
    // for (int i = 0; i < distances.size(); i++){
    //   #pragma omp ordered
    //   for (int j = 0; j < k; j++){
    //   filestream  << i 
    //       << ", "<< distances[i][j].index_a << ", "<< distances[i][j].index_b << ", "<< distances[i][j].distance << "\n";
    //   }
    // }
    
    filestream.close(); 
    auto end = chrono::steady_clock::now();
    cout << "Elapsed time in seconds: "
          <<chrono::duration_cast<chrono::milliseconds>(end-start).count()
          <<" sec";

    return 0;
}