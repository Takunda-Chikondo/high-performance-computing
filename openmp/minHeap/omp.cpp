#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <vector>
#include <sstream> 
#include <algorithm>
#include <math.h>
#include <chrono>
#include <omp.h>
#include "minHeap.cpp"
#include "plugin/dcdplugin.c"

using namespace std;

vector<int> get_all_indices(string indices_list){
	vector <string> vect;
	vector <int> output;
	std::size_t found;
	int start, end;
    string temp;

    std::stringstream ss(indices_list);
    
    while(getline(ss, temp, ',')) { 
        vect.push_back(temp); 
    } 
      
    for(int i = 0; i < vect.size(); i++){
    	found  = vect[i].find('-');
    	if (found!=std::string::npos){
    		start = stoi(vect[i].substr(0,found));
    		end = stoi(vect[i].substr(found+1, vect[i].size()-1));

    		for (int i = start; i <= end; ++i){
    			output.push_back(i);
    		}
    	}
    	else{
    		output.push_back(stoi(vect[i]));
    	}
    }
    return output;
}

MinHeap create_min_heap(vector<int> vect_a, vector<int> vect_b, float *coords){
	double x, y, z;
	Distance_Pair pair;
    MinHeap heap (vect_a.size()*vect_b.size());

	for (int i = 0; i < vect_a.size(); i++){
		for (int j = 0; j < vect_b.size(); j++){

			x = *(coords + vect_a[i]*3) - *(coords + vect_b[j]*3);
			y = *(coords + (vect_a[i]*3)+1) - *(coords + (vect_b[j]*3)+1);
			z = *(coords + (vect_a[i]*3)+2) - *(coords + (vect_b[j]*3)+2);
			pair.distance = sqrt(x*x + y*y + z*z);
			pair.index_a = vect_a[i];
			pair.index_b = vect_b[j];
            
            heap.insertKey(pair);
		}

	}
  return heap;
}

int main(int argc, char *argv[]) {
	const auto arg = std::string{"-i"};
	vector<int> vect_a, vect_b;
	void *v;
	dcdhandle *dcd;
	vector <Distance_Pair> min_distances;
    vector <Distance_Pair> distances;
	string arr[4];
	int natoms, arrSize = 0;

	
    for (int i = 0; i < argc; ++i){
    	if(i >0 && argv[i-1] == arg){

    	string x;
			ifstream inFile;

			inFile.open(argv[i]);
			if (!inFile) {
				cout << "Unable to open file";
				exit(1); // terminate with error
			}

			while (inFile >> x) {
				if(inFile.eof()){
					arr[arrSize++] = x;
					break;
				}
				arr[arrSize++] = x;
			}
			inFile.close(); 
		} 
    }

    string path_to_char = *(arr);
    int k = stoi(*(arr+1));
    string a_indices = *(arr+2);
    string b_indices = *(arr+3);

    #pragma omp parallel
    #pragma omp single nowait
    {
        #pragma omp task
        vect_a = get_all_indices(a_indices);
        #pragma omp task
        vect_b = get_all_indices(b_indices);
        #pragma omp taskwait
    }
 	
	const char *path = path_to_char.c_str();
	v = open_dcd_read(path, "dcd", &natoms);
	if (!v) {
      fprintf(stderr, "main) open_dcd_read failed for file %s\n", *argv);
      return 1;
    }

    dcd = (dcdhandle *)v;
    auto start = chrono::steady_clock::now();

    ofstream filestream;
    filestream.open("data/main_results.txt");
    molfile_timestep_t timestep;
    Distance_Pair pair;
    timestep.coords = (float *)malloc(3*sizeof(float)*natoms);


    #pragma omp parallel for
    for (int i=0; i<dcd->nsets; i++) {

        #pragma omp critical
        {
            int rc = read_next_timestep(v, natoms, &timestep);
        }
        
        MinHeap heap = create_min_heap(vect_a, vect_b, timestep.coords);
        
        //#pragma omp ordered
        for (int j = 0; j < k; j++){
            pair = heap.extractMin();
            filestream  << i    << ", "<< pair.index_a 
                                << ", "<< pair.index_b 
                                << ", "<< pair.distance 
                                << "\n";
        }    
  		  // if (rc) {
	     //    fprintf(stderr, "error in read_next_timestep on frame %d\n", i);
	     //    return 1;
      // 	 }
  	}
    close_file_read(v);

    filestream.close();
    auto end = chrono::steady_clock::now();
    cout << "Elapsed time in seconds: "
          <<chrono::duration_cast<chrono::milliseconds>(end-start).count()
          <<" millisec";

    return 0;
}
